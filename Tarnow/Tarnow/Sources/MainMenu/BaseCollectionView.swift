//
//  BaseCollectionView.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class BaseCollectionView: UICollectionView {
    
    private let numOfColumns = 2
    private let padding: CGFloat = 0
    private let listOfItems = [
        MenuModel(type: .events, ico: UIImage(named: "event-ico")!),
        MenuModel(type: .news, ico: UIImage(named: "news-ico")!),
        MenuModel(type: .card, ico: UIImage(named: "card-ico")!),
        MenuModel(type: .places, ico: UIImage(named: "map-ico")!)
    ]
    var menuDidSelect: ((MenuType) -> Void)?
    var viewHeight: CGFloat = 100
    
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        delegate = self
        dataSource = self
        
        layer.borderColor = UIColor.lightGray.cgColor
        layer.cornerRadius = 12
        layer.borderWidth = 1
        backgroundColor = .lightGray
    }
}

extension BaseCollectionView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = listOfItems[indexPath.row].type!
        menuDidSelect?(type)
    }
}

extension BaseCollectionView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! MenuCollectionViewCell
        cell.set(model: listOfItems[indexPath.row])
        return cell
    }
}


extension BaseCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let spaceForButtons = Int(collectionView.layer.frame.size.width) - ((numOfColumns + 1) * Int(padding))
        let itemWidth: CGFloat = CGFloat((spaceForButtons) / numOfColumns)
        viewHeight = (itemWidth * 2)
        return CGSize(width: itemWidth - CGFloat(0.5), height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 0
    }
}

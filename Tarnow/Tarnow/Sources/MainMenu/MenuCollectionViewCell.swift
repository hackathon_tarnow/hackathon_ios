//
//  MenuCollectionViewCell.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class MenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var type: MenuType!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .white
    }
    
    func set(model: MenuModel) {
        type = model.type
        iconImageView.image = model.ico
        iconImageView.tintColor = Colors.blue
        titleLabel.text = type.rawValue
        titleLabel.textColor = Colors.blue
    }
}

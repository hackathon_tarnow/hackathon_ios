//
//  AppCoordinator.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


protocol Coordinator {
    func start()
}


class AppCoordinator: Coordinator {
    
    private var rootViewController: UINavigationController!
    private var window: UIWindow!
    private var menuCoordinator: MenuCoordinator!
    
    init(window: UIWindow) {
        self.window = window
        self.rootViewController = UINavigationController()
        menuCoordinator = MenuCoordinator(navCtrl: rootViewController)
    }
    
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        menuCoordinator.start()
    }
}


class MenuCoordinator: Coordinator {
    
    private var navCtrl: UINavigationController!
    private lazy var menuViewCtrl: MenuViewController = {
        return MenuViewController.instantiate()
    }()
    private lazy var eventsViewCtrl: EventsListViewController = {
        let ctrl = EventsListViewController.instantiate()
        ctrl.showDetails = { [weak self] model in
            self?.showEventDetails(model: model)
        }
        return ctrl
    }()
    
    private lazy var eventDetailsViewCtrl: EventDetailsViewController = {
        let ctrl = EventDetailsViewController.instantiate()
        return ctrl
    }()
    
    private lazy var newsViewCtrl: NewsListViewController = {
        let ctrl = NewsListViewController.instantiate()
        ctrl.showDetails = { [weak self] model in
            self?.showNewsDetails(model: model)
        }
        return ctrl
    }()
    
    private lazy var newsDetailsViewCtrl: NewsDetailsViewController = {
        let ctrl = NewsDetailsViewController.instantiate()
        return ctrl
    }()
    
    private lazy var placesViewCtrl: PlacesViewController = {
        return PlacesViewController.instantiate()
    }()
    
    private lazy var grCodeViewCtrl: QRScannerController = {
        return QRScannerController.instantiate()
    }()
    
    private lazy var reporterViewCtrl: ReportViewController = {
       return ReportViewController.instantiate()
    }()
    
    private lazy var reportsListViewCtrl: ReportsListViewController = {
       return ReportsListViewController.instantiate()
    }()
    
    private lazy var searchViewCtrl: SearchViewController = {
        let ctrl = SearchViewController.instantiate()
        ctrl.showDetailsFor = { [weak self] model in
            self?.showDetailsAfterSearch(model: model)
        }
        return ctrl
    }()
    
    
    init(navCtrl: UINavigationController) {
        self.navCtrl = navCtrl
    }
    
    func start() {
        let ctrl = menuViewCtrl
        menuViewCtrl.navigateTo = { [weak self] type in
            var ctrl: UIViewController!
            
            switch type {
            case .news:
                ctrl = self?.newsViewCtrl
                break;
            case .events:
                ctrl = self?.eventsViewCtrl
                break;
            case .places:
                ctrl = self?.placesViewCtrl
                break;
            case .card:
                ctrl = self?.reportsListViewCtrl
                break;
            }
            self?.navCtrl?.pushViewController(ctrl, animated: true)
        }
        menuViewCtrl.showQRCodeCtrl = { [weak self] in
            guard let ctrl = self?.grCodeViewCtrl else { return }
            ctrl.onQRCodeDetected = { [weak self] placeId in
                ctrl.dismiss(animated: true, completion: {
                    self?.openPlaceViewController(id: placeId)
                })
            }
            self?.navCtrl.present(ctrl, animated: true, completion: nil)
        }
        menuViewCtrl.searchText = { [weak self] text in
            if let ctrl = self?.searchViewCtrl {
                ctrl.textToSearch = text
                self?.navCtrl.pushViewController(ctrl, animated: true)
            }
        }
        navCtrl.pushViewController(ctrl, animated: true)
    }
    
    private func openPlaceViewController(id: Int) {
        let ctrl = placesViewCtrl
        ctrl.focusOn(id: id)
        navCtrl.pushViewController(ctrl, animated: true)
        ctrl.navigationController?.navigationBar.isHidden = false
    }
    
    private func showEventDetails(model: EventListModel) {
        let ctrl = eventDetailsViewCtrl
        ctrl.model = model
        navCtrl.pushViewController(ctrl, animated: true)
    }
    
    private func showNewsDetails(model: NewsListModel) {
        let ctrl = newsDetailsViewCtrl
        ctrl.model = model
        navCtrl.pushViewController(ctrl, animated: true)
    }
    
    private func showDetailsAfterSearch(model: SearchModel) {
        switch model.type {
            case "PLACE":
                openPlaceViewController(id: model.objectId)
                break
            case "EVENT":
                eventsViewCtrl.idToRedirect = model.objectId
                navCtrl.pushViewController(eventsViewCtrl, animated: true)
                break
            case "NEWS":
                newsViewCtrl.idToRedirect = model.objectId
                navCtrl.pushViewController(newsViewCtrl, animated: true)
                break
            case "REPORT":
                navCtrl.pushViewController(reportsListViewCtrl, animated: true)
                break
            default:
                return
        }
        searchViewCtrl.removeFromParent()
    }
}

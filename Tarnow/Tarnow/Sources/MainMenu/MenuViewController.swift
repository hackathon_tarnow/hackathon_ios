//
//  MenuViewController.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, StoryboardInstantiable {

    @IBOutlet weak var menuCollectionView: BaseCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    
    var navigateTo: ((MenuType) -> Void)?
    var showQRCodeCtrl: (() -> Void)?
    var searchText: ((String) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTextField.delegate = self
        menuCollectionView.menuDidSelect = { [weak self] type in
            self?.navigate(by: type)
        }
        menuCollectionView.performBatchUpdates({
        }) { (done) in
            self.collectionViewHeightConstraint.constant = self.menuCollectionView.viewHeight
        }
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    private func navigate(by type: MenuType) {
        navigationController?.navigationBar.isHidden = false
        navigateTo?(type)
    }
    
    @IBAction func qrcodeButtonTapped(_ sender: Any) {
        showQRCodeCtrl?()
    }
}

extension MenuViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, text.count > 3 {
            searchText?(text)
            textField.text = ""
            textField.resignFirstResponder()
        }
        return true
    }
}


protocol StoryboardInstantiable: NSObjectProtocol {
    associatedtype MyType
    static var defaultFileName: String { get }
    static func instantiate(_ bundle: Bundle?) -> MyType
}

extension StoryboardInstantiable where Self: UIViewController {
    static var defaultFileName: String {
        return NSStringFromClass(Self.self).components(separatedBy: ".").last!
    }
    
    static func instantiate(_ bundle: Bundle? = nil) -> Self {
        let fileName = defaultFileName
        let sb = UIStoryboard(name: "Main", bundle: bundle)
        return sb.instantiateViewController(withIdentifier: fileName) as! Self
    }
}

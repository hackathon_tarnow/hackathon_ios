//
//  MenuType.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


enum MenuType: String {
    case news = "Aktualności"
    case events = "Wydarzenia"
    case places = "Miejsca"
    case card = "Awarie i zdarzenia"
}

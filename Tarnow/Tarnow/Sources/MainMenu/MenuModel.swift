//
//  MenuModel.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


struct MenuModel {
    var type: MenuType!
    var ico: UIImage!
    
    init(type: MenuType, ico: UIImage) {
        self.type = type
        self.ico = ico
    }
}

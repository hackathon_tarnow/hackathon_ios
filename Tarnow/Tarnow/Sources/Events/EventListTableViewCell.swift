//
//  EventListTableViewCell.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class EventListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var eventIcoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func set(model: EventListModel) {
        titleLabel.text = model.name
        dateLabel.text = model.eventDate
        eventIcoImageView.get(filename: model.fileName)
    }
}

//
//  CategoryType.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


enum CategoryType: String {
    case event = "Wydarzenie"
}

//
//  EventDetailsViewController.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import MapKit


class EventDetailsViewController: UIViewController, StoryboardInstantiable {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    
    private let repository = EventRepository()
    private var location: CLLocationCoordinate2D? {
        if let model = self.model {
            return CLLocationCoordinate2D(latitude: model.latitude!, longitude: model.longitude!)
        }
        return nil
    }
    var model: EventListModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        updateViews()
    }
    
    private func updateViews() {
        if let model = self.model {
            titleLabel.text = model.name
            dateLabel.text = model.eventDate
            contentTextView.text = model.eventDescription
            imageView.get(filename: model.fileName)
        }
    }
    
    private func openMap(location: CLLocationCoordinate2D?) {
        if let lat = location?.latitude, let lng = location?.longitude {
            let url = NSURL(string: "http://maps.apple.com/?q=\(lat),\(lng)")!
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func showMapButtonTapped(_ sender: Any) {
        openMap(location: location)
    }
}

//
//  EventsListViewController.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit

class EventsListViewController: UITableViewController, StoryboardInstantiable {

    var repository = EventRepository()
    var listOfItems = [EventListModel]()
    var showDetails: ((EventListModel) -> Void)?
    var idToRedirect: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableView.automaticDimension
        title = "Wydarzenia"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchList()
    }
    
    func fetchList() {
        repository.onEventListFetched = { [weak self] list in
            self?.listOfItems = list
            self?.tableView.reloadData()
            self?.redirectIfNeeded()
        }
        repository.fetchAllEvents()
    }
    
    private func redirectIfNeeded() {
        guard let id = idToRedirect else { return }
        if let model = listOfItems.filter({$0.id == id}).first {
            showDetails?(model)
            idToRedirect = nil
        }
    }
}

extension EventsListViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventListTableViewCell") as! EventListTableViewCell
        cell.set(model: listOfItems[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = listOfItems[indexPath.row]
        showDetails?(model)
    }
}

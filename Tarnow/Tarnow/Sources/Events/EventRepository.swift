//
//  EventRepository.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


class EventRepository {
    
    var onEventListFetched: (([EventListModel]) -> Void)?
    var onEventFetched: ((EventListModel) -> Void)?
    
    
    func fetchAllEvents() {
        HTTPClient.shared?.GET(URLString: "events", success: { [weak self] json in
            do {
                let list: [EventListModel] = try JSONAdapter().decodeToArray(from: json)
                self?.onEventListFetched?(list)
            } catch {
                Logger.error(error.localizedDescription)
            }
        }, failure: { (error) in
            self.onEventListFetched?([])
            Logger.error(error.localizedDescription)
        })
    }
}

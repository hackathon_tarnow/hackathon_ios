//
//  EventListModel.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


class EventListModel: Codable {
    var id: Int!
    var name: String!
    var eventDate: String!
    var eventDescription: String!
    var fileName: String!
    var latitude: Double?
    var longitude: Double?
}



class ReportListModel: Codable {
    var id: Int!
    var title: String!
    var application: String!
    var file: String!
}

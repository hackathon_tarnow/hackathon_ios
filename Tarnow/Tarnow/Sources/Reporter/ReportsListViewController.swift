//
//  ReportsListViewController.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class ReportsListViewController: UITableViewController, StoryboardInstantiable {
    
    private let repository = ReporterRepository()
    private var listOfItems = [ReportListModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableView.automaticDimension
        
        repository.onListFetched = { [weak self] list in
            self?.listOfItems = list
            self?.tableView.reloadData()
        }
        repository.fetchAll()
        title = "Zdarzenia"
        
        addRightButton()
    }
    
    func addRightButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Zgłoś", style: .done, target: self, action: #selector(reportButtonTapped))
    }
    
    @objc private func reportButtonTapped() {
        let ctrl = ReportViewController.instantiate()
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    @IBAction func onPullToRefresh(_ sender: Any) {
        repository.fetchAll()
        self.refreshControl?.endRefreshing()
    }
}


extension ReportsListViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportListTableViewCell") as! ReportListTableViewCell
        cell.set(model: listOfItems[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


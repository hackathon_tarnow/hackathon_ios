//
//  ReportViewController.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import CoreLocation


class ReportViewController: UIViewController, StoryboardInstantiable {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var textTextField: UITextField!
    @IBOutlet weak var textTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    private var locationManager:CLLocationManager!
    
    private let minCharacters = 3
    private let repository = ReporterRepository()
    private var reportTitle: String! = ""
    private var text: String! = ""
    private var selectedImage: UIImage?
    private var fileName: String! = ""
    private var userLocation: CLLocation?
    private var userLocationEnabled: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressView.isHidden = true
        sendButton.isEnabled = false
        textTextView.delegate = self
        textTextView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.35).cgColor
        textTextView.layer.cornerRadius = 5
        textTextView.layer.borderWidth = 1
        textTextField.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapped)))
        
        sendButton.setBackgroundColor(color: Colors.blue, forState: .normal)
        sendButton.setBackgroundColor(color: Colors.blue.withAlphaComponent(0.25), forState: .disabled)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc private func onViewTapped() {
        view.endEditing(true)
    }
    
    private func imageSelected(image: UIImage, filename: String) {
        self.fileName = filename
        selectedImage = image
        photoImageView.image = selectedImage
        chooseButton.isHidden = true
    }
    
    @IBAction func chooseButtonTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self]  image, filename in
            self?.imageSelected(image: image, filename: filename)
        }
    }
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        progressView.isHidden = false
        view.isUserInteractionEnabled = false
        repository.onProgress = { [weak self] progress in
            self?.progressView.progress = Float(progress)
        }
        repository.onFinish = { [weak self] error in
            if let err = error {
                Logger.error(err.localizedDescription)
            }
            self?.view.isUserInteractionEnabled = true
            self?.progressView.isHidden = true
            self?.showSuccessAlert()
        }
        if !userLocationEnabled {
            userLocation = nil
        }
        repository.send(title: reportTitle!, text: text!, filename: fileName, image: selectedImage, userLocation: userLocation)
    }
    
    @IBAction func myLocationSwitchTapped(_ sender: UISwitch) {
        userLocationEnabled = sender.isOn
    }
    
    private func showSuccessAlert() {
        presentAlertWithTitle(title: "Wysłano", message: "Zgłoszenie zostało przyjęte. Dziękujemy.", options: "OK") { _ in
            self.resetView()
        }
    }
    
    private func resetView() {
        self.reportTitle = ""
        self.text = ""
        self.selectedImage = nil
        self.fileName = ""
        self.textTextView.text = ""
        self.textTextField.text = ""
        self.photoImageView.image = nil
        self.chooseButton.isHidden = false
        self.updateSendButtonState()
    }
    
    private func updateSendButtonState() {
        if reportTitle.count > minCharacters && text.count > minCharacters {
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
    }
}

extension ReportViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0] as CLLocation
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}


extension ReportViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        text = textView.text
        updateSendButtonState()
    }
}


extension ReportViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        reportTitle = (textField.text ?? "") + string
        updateSendButtonState()
        return true
    }
}


extension UIViewController {
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
}

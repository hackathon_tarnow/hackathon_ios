//
//  ReportListTableViewCell.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class ReportListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var applicationLabel: UILabel!
    @IBOutlet weak var reportImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func set(model: ReportListModel) {
        titleLabel.text = model.title
        applicationLabel.text = model.application
        reportImageView.get(filename: model.file)
    }
}

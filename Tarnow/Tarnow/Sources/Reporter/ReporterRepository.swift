//
//  ReporterRepository.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import Alamofire
import MapKit


class ReporterRepository {
    
    var onProgress: ((Double) -> Void)?
    var onFinish: ((Error?) -> Void)?
    var onListFetched: (([ReportListModel]) -> Void)?
    
    
    func send(title: String, text: String, filename: String, image: UIImage?, userLocation: CLLocation?) {
        let lat = userLocation?.coordinate.latitude
        let lng = userLocation?.coordinate.longitude
        let params: [String: Any] = ["application": text,
                      "file": filename,
                      "title": title,
                      "latitude": lat,
                      "longitude": lng]
        
        HTTPClient.shared?.POST(URLString: "reports", parameters: params, encoding: JSONEncoding.default, success: { (json) in
          self.requestWith(imageData: image?.pngData(), filename: filename, parameters: [:])
        }, failure: { (error) in
            self.onFinish?(error)
        })
    }
    
    private func requestWith(imageData: Data?, filename: String, parameters: [String : Any]) {
        let url = ConfigurationManager.shared.fileUploadURLString
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData {
                multipartFormData.append(data, withName: "file", fileName: filename, mimeType: "image/png")
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    self.onProgress?(progress.fractionCompleted)
                })
                upload.responseJSON { response in
                    if let err = response.error {
                        self.onFinish?(err)
                        return
                    }
                    self.onFinish?(nil)
                }
            case .failure(let error):
                self.onFinish?(error)
            }
        }
    }
    
    func fetchAll() {
        
        HTTPClient.shared?.GET(URLString: "reports", success: { [weak self] json in
            do {
                let list: [ReportListModel] = try JSONAdapter().decodeToArray(from: json)
                self?.onListFetched?(list)
            } catch {
                Logger.error(error.localizedDescription)
            }
        }, failure: { (error) in
            self.onListFetched?([])
        })
    }
}

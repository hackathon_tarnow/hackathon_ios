//
//  JSONTaskComponents.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Alamofire

public protocol JSONTaskComponents {
    var url: URLConvertible { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
//    var success: (([String: Any]) -> Void)? { get }
    var success: (([Any]) -> Void)? { get }
    var failure: ((NetworkError) -> Void)? { get }
}

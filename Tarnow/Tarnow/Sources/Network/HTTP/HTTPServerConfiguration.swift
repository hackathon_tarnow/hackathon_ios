//
//  HTTPServerConfiguration.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation

public struct HTTPServerConfiguration {

    public var host: String?
    public var port: String?
    public var serverProtocol: String?
    public var applicationContext: String?

    public var serverAddress: URL? {
        guard self.serverProtocol != nil, self.host != nil else {
            return nil
        }
        if self.port != nil {
            return URL(string: "\(self.serverProtocol!)://\(self.host!):\(self.port!)")
        }
        return URL(string: "\(self.serverProtocol!)://\(self.host!)")
    }


    public static var `default`: HTTPServerConfiguration = {

        var serverConfiguration = HTTPServerConfiguration()

        serverConfiguration.host = ConfigurationManager.shared.serverHost
        serverConfiguration.port = ConfigurationManager.shared.serverPort
        serverConfiguration.serverProtocol = ConfigurationManager.shared.serverProtocol
        serverConfiguration.applicationContext = ConfigurationManager.shared.applicationContext

        return serverConfiguration;
    }()


    public init() {
    }

    public init(host: String?, port: String?, serverProtocol: String?, applicationContext: String?) {

        self.host = host
        self.port = port
        self.serverProtocol = serverProtocol
        self.applicationContext = applicationContext
    }
}

//
//  OAuth2Client.swift
//  Soou.me
//
//  Created by Robert Mietelski on 15.01.2019.
//  Copyright © 2019 altconnect. All rights reserved.
//

import Alamofire

class OAuth2Client {

    // MARK: - Public attributes

    public var baseURL: URL {
        return self.manager.baseURL
    }

    public var applicationContext: String? {
        return self.manager.applicationContext
    }

    public var serverAddress: URL {
        return self.manager.serverAddress
    }

    public var clientId: String {
        return self.manager.clientID
    }

    public var clientSecret: String {
        return self.manager.clientSecret
    }

    public var credential: OAuth2Credential? {
        return self.manager.credential
    }

    static public let shared = OAuth2Client(timeoutInterval: 30.0)

    // MARK: - Private attributes

    private var manager: OAuth2Manager

    // MARK: - Initialization

    private convenience init?(timeoutInterval: TimeInterval) {

        guard let baseURL = HTTPServerConfiguration.default.serverAddress,
              let clientID = ConfigurationManager.shared.clientId,
              let clientSecret = ConfigurationManager.shared.clientSecret else {
            return nil
        }

        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = timeoutInterval
        sessionConfiguration.timeoutIntervalForResource = timeoutInterval;

        self.init(baseURL: baseURL,
                  applicationContext: HTTPServerConfiguration.default.applicationContext,
                  clientID: clientID,
                  clientSecret: clientSecret)
    }

    public init?(baseURL: URL,
                 applicationContext: String?,
                 clientID: String,
                 clientSecret: String,
                 sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default,
                 delegate: SessionDelegate = SessionDelegate(),
                 serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {

        guard let manager = OAuth2Manager(baseURL: baseURL,
                                          applicationContext: applicationContext,
                                          clientID: clientID,
                                          clientSecret: clientSecret,
                                          sessionConfiguration: sessionConfiguration,
                                          delegate: delegate,
                                          serverTrustPolicyManager: serverTrustPolicyManager) else {
            return nil
        }
        self.manager = manager
    }
}

// MARK: - Session operation management

extension OAuth2Client {
    private func sessionOperation(URLString: String,
                                  method: HTTPMethod,
                                  parameters: Parameters?,
                                  encoding: ParameterEncoding,
                                  headers: HTTPHeaders? = nil,
                                  success: (([String: Any]) -> Void)?,
                                  failure: ((NetworkError) -> Void)?) -> URLSessionOperation {

        let refreshToken: (NetworkError) -> Void = { error in

            if error.statusCode == HTTPStatus.unauthorized.rawValue {
                let pendingOperation = self.manager.sessionOperation(URLString: URLString,
                                                                     method: method,
                                                                     parameters: parameters,
                                                                     encoding: encoding,
                                                                     headers: headers,
                                                                     success: success,
                                                                     failure: failure)

                self.manager.pendingOperations.append(pendingOperation)
                if let refreshToken = self.manager.credential?.refreshToken {

                    self.refreshToken(numberOfAttempts: 3,
                                      refreshToken: refreshToken,
                                      success: { credential in
                                          let headers: HTTPHeaders?

                                          if let tokenType = credential.tokenType, let accessToken = credential.accessToken {
                                              headers = ["Authorization": "\(tokenType) \(accessToken)"]
                                          } else {
                                              headers = nil
                                          }

                                          // Wznawiam oczekujące operację
                                          self.manager.resumePendingOperations(headers: headers) {
                                          }
                                      }, failure: failure)
                } else if let completionHandler = failure {
                    completionHandler(error)
                }
            } else if let completionHandler = failure {
                completionHandler(error)
            }
        }

        return self.manager.sessionOperation(URLString: URLString,
                                             method: method,
                                             parameters: parameters,
                                             encoding: encoding,
                                             headers: headers,
                                             success: success,
                                             failure: refreshToken)
    }

    private func refreshToken(numberOfAttempts: Int,
                              refreshToken: String,
                              success: ((OAuth2Credential) -> Void)?,
                              failure: ((NetworkError) -> Void)?) {

        var parameters: Parameters = Parameters()
        parameters["grant_type"] = OAuth2GrantType.refreshToken.rawValue
        parameters["refresh_token"] = refreshToken
        parameters["scope"] = ConfigurationManager.shared.scope

        self.authenticate(URLString: "oauth/token",
                          parameters: parameters,
                          success: success,
                          failure: { error in

                              if numberOfAttempts > 1 {
                                  self.refreshToken(numberOfAttempts: numberOfAttempts - 1,
                                                    refreshToken: refreshToken,
                                                    success: success,
                                                    failure: failure)
                              } else if let completionHandler = failure {
                                  let networkError = HTTPError(statusCode: HTTPStatus.unauthorized.rawValue,
                                                               userDescription: error.localizedDescription)
                                  completionHandler(networkError)
                              }
                          })
    }
}

// MARK: - Authentication

extension OAuth2Client {
    public func authenticate(URLString: String,
                             parameters: Parameters?,
                             success: ((OAuth2Credential) -> Void)?,
                             failure: ((NetworkError) -> Void)?) {

        self.manager.startOperationImmediately = false
        let operation = self.manager.authenticateOperation(URLString: URLString,
                                                           parameters: parameters,
                                                           success: { [weak self] credential in
                                                               self?.manager.startOperationImmediately = true

                                                               if let completionHandler = success {
                                                                   completionHandler(credential)
                                                               }
                                                           },
                                                           failure: { [weak self] error in
                                                               self?.manager.startOperationImmediately = true

                                                               if let completionHandler = failure {
                                                                   completionHandler(error)
                                                               }
                                                           })
        self.manager.execute(sessionOperation: operation)
    }

    public func authenticate(username: String,
                             password: String,
                             deviceId: String,
                             scope: String,
                             success: ((OAuth2Credential) -> Void)?,
                             failure: ((NetworkError) -> Void)?) {

        var parameters: Parameters = Parameters()
        parameters["grant_type"] = OAuth2GrantType.password.rawValue
        parameters["username"] = username
        parameters["password"] = password
        parameters["device_id"] = deviceId
        parameters["scope"] = scope

        self.authenticate(URLString: "oauth/token", parameters: parameters, success: success, failure: failure)
    }

    public func authenticate(facebookToken: String,
                             deviceId: String,
                             success: ((OAuth2Credential) -> Void)?,
                             failure: ((NetworkError) -> Void)?) {

        var parameters: Parameters = Parameters()
        parameters["facebookToken"] = facebookToken
        parameters["device_id"] = deviceId

        self.manager.useHTTPBasicAuthentication = true
        self.authenticate(URLString: "/sooume/users/login-fb",
                          parameters: parameters,
                          success: { [weak self] credential in
                              self?.manager.useHTTPBasicAuthentication = false

                              if let completionHandler = success {
                                  completionHandler(credential)
                              }
                          },
                          failure: { [weak self] error in
                              self?.manager.useHTTPBasicAuthentication = false

                              if let completionHandler = failure {
                                  completionHandler(error)
                              }
                          })
    }

    func invalidateSession() {
        self.manager.invalidateSession()
    }
}

// MARK: - HTTP support

extension OAuth2Client {
    public func GET(URLString: String,
                    parameters: Parameters? = nil,
                    encoding: ParameterEncoding = JSONEncoding.default,
                    success: (([String: Any]) -> Void)?,
                    failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .get, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }

    public func HEAD(URLString: String,
                     parameters: Parameters? = nil,
                     encoding: ParameterEncoding = JSONEncoding.default,
                     success: (([String: Any]) -> Void)?,
                     failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .head, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }

    public func POST(URLString: String,
                     parameters: Parameters? = nil,
                     encoding: ParameterEncoding = JSONEncoding.default,
                     success: (([String: Any]) -> Void)?,
                     failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .post, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }

    public func PUT(URLString: String,
                    parameters: Parameters? = nil,
                    encoding: ParameterEncoding = JSONEncoding.default,
                    success: (([String: Any]) -> Void)?,
                    failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .put, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }

    public func PATCH(URLString: String,
                      parameters: Parameters? = nil,
                      encoding: ParameterEncoding = JSONEncoding.default,
                      success: (([String: Any]) -> Void)?,
                      failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .patch, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }

    public func DELETE(URLString: String,
                       parameters: Parameters? = nil,
                       encoding: ParameterEncoding = JSONEncoding.default,
                       success: (([String: Any]) -> Void)?,
                       failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .delete, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }
}

//
//  OAuth2Credential.swift
//  Soou.me
//
//  Created by Gienadij Mackiewicz on 13.07.2018.
//

import Foundation

public class OAuth2Credential: NSObject, NSCoding, Codable {
    
    public var accessToken: String?
    public var tokenType: String?
    public var refreshToken: String?
    public var expirationDate: Date?
    
    public var isExpired: Bool {
        return self.expirationDate?.compare(Date()) == .orderedAscending
    }
    
    // MARK: - Konstruktory
    public override init() {
        super.init()
    }
    
    public init(accessToken: String?, tokenType: String?, refreshToken: String? = nil, expirationDate: Date? = nil) {
        super.init()
        
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.refreshToken = refreshToken
        self.expirationDate = expirationDate
    }
    
    // MARK: - Implementacja protokołu NSCoding
    public required init?(coder aDecoder: NSCoder) {
        super.init()
        
        self.accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String
        self.tokenType = aDecoder.decodeObject(forKey: "tokenType") as? String
        self.refreshToken = aDecoder.decodeObject(forKey: "refreshToken") as? String
        self.expirationDate = aDecoder.decodeObject(forKey: "expirationDate") as? Date
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.accessToken, forKey: "accessToken")
        aCoder.encode(self.tokenType, forKey: "tokenType")
        aCoder.encode(self.refreshToken, forKey: "refreshToken")
        aCoder.encode(self.expirationDate, forKey: "expirationDate")
    }
}

extension OAuth2Credential {
    
    // MARK: - Zapis danych
    public static func store(credential: OAuth2Credential, identifier: String) {
        UserDefaults.standard.set(object: credential, forKey: identifier)
        UserDefaults.standard.synchronize()
    }
    
    public static func retrieveCredential(identifier: String) -> OAuth2Credential? {
        return UserDefaults.standard.object(OAuth2Credential.self, with: identifier)
    }
    
    public static func deleteCredential(identifier: String) {
        UserDefaults.standard.removeObject(forKey: identifier)
        UserDefaults.standard.synchronize()
    }
}


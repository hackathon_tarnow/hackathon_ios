//
//  OAuth2Manager.swift
//  Soou.me
//
//  Created by Gienadij Mackiewicz on 13.07.2018.
//

import Alamofire

public enum OAuth2GrantType: String {
    case clientCredentials = "client_credentials"
    case password = "password"
    case authorizationCode = "authorization_code"
    case refreshToken = "refresh_token"
}

public class OAuth2Manager: NetworkAccessManager {

    // MARK: - Public attributes
    
    public private(set) var clientID: String
    public private(set) var clientSecret: String
    
    public private(set) var credential = OAuth2Credential.retrieveCredential(identifier: OAuth2Manager.credentialIdentifier) {
        didSet {
            if let credential = credential {
                OAuth2Credential.store(credential: credential, identifier: OAuth2Manager.credentialIdentifier)
            }
            else {
                OAuth2Credential.deleteCredential(identifier: OAuth2Manager.credentialIdentifier)
            }
        }
    }
    
    public var useHTTPBasicAuthentication = false
    
    // MARK: - Private attributes
    
    private static let credentialIdentifier = "\(Bundle.main.bundleIdentifier!).OAuth2Credential"
    
    // MARK: - Initialization
    
    public init?(baseURL: URL,
                 applicationContext: String?,
                 clientID: String,
                 clientSecret: String,
                 sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default,
                 delegate: SessionDelegate = SessionDelegate(),
                 serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        
        self.clientID = clientID
        self.clientSecret = clientSecret
        
        super.init(baseURL: baseURL,
                   applicationContext: applicationContext,
                   sessionConfiguration: sessionConfiguration,
                   delegate: delegate,
                   serverTrustPolicyManager: serverTrustPolicyManager)
    }

    // MARK: - Access methods
    
    public override func sessionOperation(URLString: String,
                                          method: HTTPMethod,
                                          parameters: Parameters? = nil,
                                          encoding: ParameterEncoding = JSONEncoding.default,
                                          headers: HTTPHeaders? = nil,
                                          success: (([String: Any]) -> Void)?,
                                          failure: ((NetworkError) -> Void)?) -> URLSessionOperation {
        
        var requestHeaders = headers ?? HTTPHeaders()
        
        if let tokenType = self.credential?.tokenType, let accessToken = self.credential?.accessToken {
            requestHeaders["Authorization"] = "\(tokenType) \(accessToken)"
        }
        
        return super.sessionOperation(URLString: URLString,
                                      method: method,
                                      parameters: parameters,
                                      encoding: encoding,
                                      headers: requestHeaders,
                                      success: success,
                                      failure: failure)
    }
}

// MARK: - Session management

extension OAuth2Manager {
    public func authenticateOperation(URLString: String,
                                      parameters: Parameters?,
                                      success: ((OAuth2Credential) -> (Void))?,
                                      failure: ((NetworkError) -> (Void))?) -> URLSessionOperation {
        
        var requestParameters = parameters ?? Parameters()
        var headers = HTTPHeaders()
        
        if !self.useHTTPBasicAuthentication {
            requestParameters["client_id"] = self.clientID
            requestParameters["client_secret"] = self.clientSecret

            headers["Authorization"] = "Basic \("\(self.clientID):\(self.clientSecret)".data(using: .utf8)!.base64EncodedString())"
        }
        
        let success: ([String: Any]) -> Void = { json in
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                let credential = OAuth2Credential()
                
                credential.accessToken = json["access_token"] as? String
                credential.tokenType = json["token_type"] as? String
                credential.refreshToken = json["refresh_token"] as? String
                
                if let timeInterval = json["expires_in"] as? Double {
                    credential.expirationDate = Date(timeIntervalSinceNow: timeInterval)
                }
                
                DispatchQueue.main.sync {
                    self.credential = credential
                    
                    if let completionHandler = success {
                        completionHandler(credential)
                    }
                }
            }
        }
        
        return super.sessionOperation(URLString: URLString,
                                      method: .post,
                                      parameters: parameters,
                                      encoding: URLEncoding.default,
                                      headers: headers,
                                      success: success,
                                      failure: failure)
    }
    
    public func invalidateSession() {
        self.credential = nil
    }
}

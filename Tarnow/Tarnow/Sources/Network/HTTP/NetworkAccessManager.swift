import Alamofire


public class NetworkAccessManager {
    
    public var baseURL: URL
    public var applicationContext: String?
    public var pendingOperations: [URLSessionOperation] = []
    public var startOperationImmediately: Bool = true
    public lazy var serverAddress: URL = {
        guard self.applicationContext != nil else {
            return self.baseURL
        }
        return self.baseURL.appendingPathComponent(self.applicationContext!)
    }()
    private let operationQueue = OperationQueue()
    private var manager: SessionManager
    
    
    public init(baseURL: URL,
                applicationContext: String?,
                sessionConfiguration:URLSessionConfiguration = URLSessionConfiguration.default,
                delegate: SessionDelegate = SessionDelegate(),
                serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        
        let sessionManager = SessionManager(configuration: sessionConfiguration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        sessionManager.startRequestsImmediately = false
        
        self.baseURL = baseURL
        self.applicationContext = applicationContext
        self.manager = sessionManager
    }
    
    public func sessionOperation(URLString: String,
                                 method: HTTPMethod,
                                 parameters: Parameters?,
                                 encoding: ParameterEncoding,
                                 headers: HTTPHeaders?,
                                 success: (([Any]) -> Void)?,
                                 failure: ((NetworkError) -> Void)?) -> URLSessionOperation {
        
        let components = URLSessionTaskComponents(url: self.serverAddress.appendingPathComponent(URLString),
                                                  method: method,
                                                  parameters: parameters,
                                                  encoding: encoding,
                                                  headers: headers,
                                                  success: success,
                                                  failure: failure)
        
        return URLSessionOperation(manager: self.manager, components: components)
    }
}


extension NetworkAccessManager {
    public func execute(sessionOperation: URLSessionOperation) {
        self.operationQueue.addOperation(sessionOperation)
    }
    
    public func enqueue(sessionOperation: URLSessionOperation) {
        
        if !self.startOperationImmediately {
            self.pendingOperations.append(sessionOperation)
        } else {
            self.operationQueue.addOperation(sessionOperation)
        }
    }
    
    public func resumePendingOperations(headers: HTTPHeaders? = nil, completion: @escaping () -> Void) {
        Logger.debug("Resuming pending operations")
        
        let completionOperation = BlockOperation(block: completion)
        var operations = [URLSessionOperation]()
        
        for operation in self.pendingOperations {
            Logger.debug("Operation url: \(operation.components.url)")
            
            let updatedHeaders = operation.components.headers?.reduce(into: HTTPHeaders()) { (result, tuple) in
                if let value = headers?[tuple.key] {
                    result[tuple.key] = value
                } else {
                    result[tuple.key] = tuple.value
                }
            }
            let updatedComponents = operation.components.componentsByChanging(headers: updatedHeaders)
            let updatedOperation = URLSessionOperation(manager: operation.manager, components: updatedComponents)
            
            completionOperation.addDependency(updatedOperation)
            operations.append(updatedOperation)
        }
        OperationQueue.main.addOperation(completionOperation)
        
        self.operationQueue.addOperations(operations, waitUntilFinished: false)
        self.pendingOperations = [URLSessionOperation]()
    }
}

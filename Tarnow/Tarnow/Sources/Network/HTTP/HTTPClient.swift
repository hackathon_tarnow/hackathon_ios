//
//  HTTPClient.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Alamofire

public class HTTPClient {
    public var baseURL: URL {
        return self.manager.baseURL
    }
    public var applicationContext: String? {
        return self.manager.applicationContext
    }
    public var serverAddress: URL {
        return self.manager.serverAddress
    }
    static public let shared = HTTPClient(timeoutInterval: 30.0)
    private var manager: NetworkAccessManager


    private convenience init?(timeoutInterval: TimeInterval) {
        guard let baseURL = HTTPServerConfiguration.default.serverAddress else {
            return nil
        }

        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = timeoutInterval
        sessionConfiguration.timeoutIntervalForResource = timeoutInterval;

        self.init(baseURL: baseURL, applicationContext: HTTPServerConfiguration.default.applicationContext)
    }

    public init(baseURL: URL,
                applicationContext: String?,
                sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default,
                delegate: SessionDelegate = SessionDelegate(),
                serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {

        self.manager = NetworkAccessManager(baseURL: baseURL,
                                            applicationContext: applicationContext,
                                            sessionConfiguration: sessionConfiguration,
                                            delegate: delegate,
                                            serverTrustPolicyManager: serverTrustPolicyManager)
    }
}


extension HTTPClient {
    private func sessionOperation(URLString: String,
                                  method: HTTPMethod,
                                  parameters: Parameters?,
                                  encoding: ParameterEncoding,
                                  headers: HTTPHeaders? = nil,
                                  success: (([Any]) -> Void)?,
                                  failure: ((NetworkError) -> Void)?) -> URLSessionOperation {

        return self.manager.sessionOperation(URLString: URLString,
                                             method: method,
                                             parameters: parameters,
                                             encoding: encoding,
                                             headers: headers,
                                             success: success,
                                             failure: failure)
    }
}


extension HTTPClient {
    public func GET(URLString: String,
                    parameters: Parameters? = nil,
                    encoding: ParameterEncoding = JSONEncoding.default,
                    success: (([Any]) -> Void)?,
                    failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .get, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }

    public func POST(URLString: String,
                     parameters: Parameters? = nil,
                     encoding: ParameterEncoding = JSONEncoding.default,
                     success: (([Any]) -> Void)?,
                     failure: ((NetworkError) -> Void)?) {

        let operation = self.sessionOperation(URLString: URLString, method: .post, parameters: parameters, encoding: encoding, success: success, failure: failure)
        self.manager.enqueue(sessionOperation: operation)
    }
}

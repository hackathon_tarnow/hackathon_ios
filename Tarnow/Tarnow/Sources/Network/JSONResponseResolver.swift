//
//  JSONResponseResolver.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Alamofire


public class JSONResponseResolver: NSObject {

    public let components: JSONTaskComponents

    public init(components: JSONTaskComponents) {
        self.components = components
    }


    public func resolve(response: DataResponse<Any>, requestStartDate date: Date) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            Logger.debug("URL: \(self.components.url)")
            Logger.debug("Execution Time = \(Date().timeIntervalSince(date))")

            switch response.result {

                case .success(let data):
                    if let json = data as? [Any] {
                        Logger.debug("Response JSON: \(json)")
                        DispatchQueue.main.sync {
                            self.components.success?(json)
                        }
                    } else {
                        Logger.error("Response is not list: \(data)")
                        DispatchQueue.main.sync {
                            self.components.success?([])
                        }
                    }
                case .failure(let error):
                    Logger.error(error)
            }
        }
    }
}

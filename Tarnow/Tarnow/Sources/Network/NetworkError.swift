//
//  NetworkError.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


public protocol NetworkError: Error {
    var statusCode: Int { get }
    var developerDescription: String? { get }
}

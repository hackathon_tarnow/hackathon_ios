//
//  NewsListTableViewCell.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class NewsListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsIcoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func set(model: NewsListModel) {
        titleLabel.text = model.title
        dateLabel.text = model.data
        newsIcoImageView.get(filename: model.fileName)
    }
}


//
//  NewsDetailsViewController.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import MapKit


class NewsDetailsViewController: UIViewController, StoryboardInstantiable {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    
    var model: NewsListModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if let model = self.model {
            titleLabel.text = model.title
            dateLabel.text = model.data
            contentTextView.text = model.text
            imageView.get(filename: model.fileName)
        }
    }

}

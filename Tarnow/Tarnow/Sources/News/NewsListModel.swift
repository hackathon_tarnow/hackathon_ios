//
//  NewsListModel.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


class NewsListModel: Codable {
    var id: Int!
    var title: String!
    var data: String!
    var text: String!
    var fileName: String!
}

//
//  NewsRepository.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


class NewsRepository {
    
    var onNewsListFetched: (([NewsListModel]) -> Void)?
    var onNewsFetched: ((NewsListModel) -> Void)?
    
    
    func fetchAllNews() {
        HTTPClient.shared?.GET(URLString: "news", success: { [weak self] json in
            do {
                let list: [NewsListModel] = try JSONAdapter().decodeToArray(from: json)
                self?.onNewsListFetched?(list)
            } catch {
                Logger.error(error.localizedDescription)
            }
            }, failure: { (error) in
                self.onNewsListFetched?([])
                Logger.error(error.localizedDescription)
        })
    }
}



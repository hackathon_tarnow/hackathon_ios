//
//  PlacesViewController.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import MapKit


class PlacesViewController: UIViewController, StoryboardInstantiable {
    
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var detailsView: UIView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var contentTextView: UITextView!
    @IBOutlet private weak var cityCardLabel: UILabel!
    private var repository = PlacesRepository()
    private var listOfAnnotations = [Annotation]()
    private var selectdId: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        repository.onListFetched = { [weak self] list in
            self?.setListOfAnnotations(list: list)
        }
        repository.fetchAll()
        detailsView.isHidden = true
        let mapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onMapTapped))
        mapView.addGestureRecognizer(mapGestureRecognizer)
        title = "Miejsca"
    }
    
    @objc private func onMapTapped() {
        detailsView.isHidden = true
    }
    
    func focusOn(id: Int) {
        if let model = listOfAnnotations.filter({$0.id == id}).first {
            self.showDetails(model: model)
        } else {
            selectdId = id
            repository.fetchAll()
        }
    }
    
    private func setListOfAnnotations(list: [Annotation]) {
        listOfAnnotations = list
        mapView.addAnnotations(list)
        mapView.showAnnotations(list, animated: true)
        if let id = selectdId {
            if let model = listOfAnnotations.filter({$0.id == id}).first {
                mapView.selectAnnotation(model, animated: true)
                self.showDetails(model: model)
            }
        }
    }
    
    private func showDetails(model: Annotation) {
        selectdId = nil
        detailsView.isHidden = false
        nameLabel.text = model.name
        contentTextView.text = model.content
        if model.cityCardAllowed ?? false {
            cityCardLabel.text = "Usługa dostępna dla Karty Miejskiej"
        } else {
            cityCardLabel.text = nil
        }
    }
}


extension PlacesViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let model = (view.annotation as? Annotation) {
            showDetails(model: model)
        }
    }
}



//
//  Annotation.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import MapKit


class Annotation: NSObject, Codable, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D {
        if let lat = latitude, let lng = longitude {
            return CLLocationCoordinate2D(latitude: lat, longitude: lng)
        }
        return CLLocationCoordinate2D(latitude: 50.0, longitude: 20.021)
    }
    var title: String? {
        return name
    }
    var subtitle: String?
    
    var id: Int!
    var name: String!
    var content: String!
    var latitude: Double?
    var longitude: Double?
    var cityCardAllowed: Bool? = false
}

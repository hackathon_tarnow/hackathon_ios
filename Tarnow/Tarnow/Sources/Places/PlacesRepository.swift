//
//  PlacesRepository.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation
import Alamofire


class PlacesRepository {
    
    var onListFetched: (([Annotation]) -> Void)?
    
    
    func fetchAll() {
        HTTPClient.shared?.GET(URLString: "places", success: { [weak self] json in
            do {
                let list: [Annotation] = try JSONAdapter().decodeToArray(from: json)
                self?.onListFetched?(list)
            } catch {
                Logger.error(error.localizedDescription)
            }
            }, failure: { (error) in
                self.onListFetched?([])
                Logger.error(error.localizedDescription)
        })
    }
}

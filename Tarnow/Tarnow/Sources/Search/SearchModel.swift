//
//  SearchModel.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class SearchModel: Codable {
    var id: Int
    var value: String!
    var objectId: Int!
    var type: String!
    
    static func typeToDisplay(type: String) -> String {
        switch type {
        case "PLACE":
            return "Miejsca"
        case "EVENT":
            return "Wydarzenia"
        case "NEWS":
            return "Aktualności"
        case "REPORT":
            return "Awarie i zdarzenia"
        default:
            return ""
        }
    }
}

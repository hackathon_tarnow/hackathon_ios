//
//  SearchTableViewCell.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func set(model: SearchModel) {
        valueLabel.text = model.value
    }
}

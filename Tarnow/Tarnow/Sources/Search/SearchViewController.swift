//
//  SearchViewController.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import Alamofire


class SearchViewController: UITableViewController, StoryboardInstantiable {
    
    private var repository = SearchRepository()
    private var listOfItems: [String?: [SearchModel]] = [:]
    var textToSearch: String!
    var showDetailsFor: ((SearchModel) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        repository.onSectionFetched = { [weak self] sections in
            self?.listOfItems = sections
            self?.tableView.reloadData()
        }
        title = "Wyniki wyszukiwania"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        repository.fetchAll(value: textToSearch)
    }
}


extension SearchViewController {
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let keys = listOfItems.map({$0.key!})
        let type = keys[section]
        return SearchModel.typeToDisplay(type: type)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let arr = listOfItems.values.map({$0})
        return arr.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arr = listOfItems.values.map({$0})
        return arr[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell") as! SearchTableViewCell
        let model = modelByIndex(indexPath: indexPath)
        cell.set(model: model)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = modelByIndex(indexPath: indexPath)
        showDetailsFor?(model)
    }
    
    private func modelByIndex(indexPath: IndexPath) -> SearchModel {
        let arr = listOfItems.values.map({$0})
        return arr[indexPath.section][indexPath.row]
    }
}



class SearchRepository {
    
    var onSectionFetched: (([String?: [SearchModel]]) -> Void)?
    
    
    func fetchAll(value: String) {
        let params: [String: Any] = ["search": value]
        
        HTTPClient.shared?.GET(URLString: "search", parameters: params, encoding: URLEncoding.default, success: { (json) in
            do {
                let list:[SearchModel] = try JSONAdapter().decodeToArray(from: json)
                let sections = list.categorise({$0.type})
                self.onSectionFetched?(sections)
            } catch {
                Logger.error(error)
            }
        }, failure: { (err) in
            Logger.error(err)
        })
    }
}


public extension Sequence {
    func categorise<U : Hashable>(_ key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var dict: [U:[Iterator.Element]] = [:]
        for el in self {
            let key = key(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}

//
//  UIViewController+Extension.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


extension UIViewController {

    func addBlurToNavBar() {
        if let navigationBar = self.navigationController?.navigationBar {
            let statusBarHeight = UIApplication.shared.statusBarFrame.height
            var blurFrame = navigationBar.bounds
            blurFrame.size.height += statusBarHeight
            blurFrame.origin.y -= statusBarHeight
            let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
            blurView.isUserInteractionEnabled = false
            blurView.frame = blurFrame
            blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            navigationBar.setBackgroundImage(UIImage(), for: .default)
            blurView.layer.zPosition = -1
            blurView.isHidden = true
            navigationBar.addSubview(blurView)
        }
    }
}

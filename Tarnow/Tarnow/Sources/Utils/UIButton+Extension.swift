//
//  UIButton+Extension.swift
//  Tarnow
//
//  Created by Daniel on 15/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


extension UIButton {
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        let minimumSize: CGSize = CGSize(width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(minimumSize)
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(origin: .zero, size: minimumSize))
        }
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.clipsToBounds = true
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    func setBackgroundColorWithHiglightOnTouch(color: UIColor) {
        self.setBackgroundColor(color: color, forState: .normal)
        self.setBackgroundColor(color: color.withAlphaComponent(0.9), forState: .highlighted)
    }
    
    func setTitleColorWithHiglightOnTouch(color: UIColor) {
        self.setTitleColor(color, for: .normal)
        self.setTitleColor(color.withAlphaComponent(0.9), for: .highlighted)
    }
}


//
//  ImageDownloader.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit
import Kingfisher


extension UIImageView {
    
    func get(filename: String?) {
        guard let filename = filename else { return }
        let prefix = ConfigurationManager.shared.imageURLString
        let url = URL(string: prefix + filename)
        
        var iv = self
        iv.kf.indicatorType = .activity
        iv.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder"),
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
    
}

//
//  Logger.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation


public final class Logger {
    public enum LogType: String {
        case debug = "🔹DEBUG: "
        case info = "✅INFO: "
        case warning = "⚠️WARNING: "
        case error = "🆘ERROR: "
        
        var shouldAppendNewLine: Bool {
            switch self {
            case .error: return true
            default: return false
            }
        }
    }
    
    class func debug(_ msg: Any?,
                     _ file: String = #file,
                     _ function: String = #function,
                     _ line: Int = #line) {
        log(message: msg ?? "", withType: LogType.debug, fileName: file, functionName: function, line: line)
    }
    
    class func info(_ msg: Any,
                    _ file: String = #file,
                    _ function: String = #function,
                    _ line: Int = #line) {
        log(message: msg, withType: LogType.info, fileName: file, functionName: function, line: line)
    }
    
    class func warning(_ msg: Any,
                       _ file: String = #file,
                       _ function: String = #function,
                       _ line: Int = #line) {
        log(message: msg, withType: LogType.warning, fileName: file, functionName: function, line: line)
    }
    
    class func error(_ msg: Any,
                     _ file: String = #file,
                     _ function: String = #function,
                     _ line: Int = #line) {
        log(message: msg, withType: LogType.error, fileName: file, functionName: function, line: line)
    }
    
    public static func log(message: Any,
                           withType type: LogType,
                           fileName: String,
                           functionName: String,
                           line: Int) {
        
        var information = "\(type.rawValue) \(fileName): \(line) \(functionName): \(message)"
        if type.shouldAppendNewLine {
            information.append("\n")
        }
        print(information)
    }
}


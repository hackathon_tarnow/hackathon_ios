//
//  Colors.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import UIKit


struct Colors {
    static let blue = UIColor(hex: "0060AA", withDefaultColor: .black)
}


extension UIColor {
    
    public convenience init(hex: String?, withDefaultColor defaultColor: UIColor) {
        if hex == nil {
            self.init(cgColor: defaultColor.cgColor)
            return
        }
        var cString:String = hex!.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

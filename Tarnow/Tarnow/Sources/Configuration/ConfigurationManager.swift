//
//  ConfigurationManager.swift
//  Tarnow
//
//  Created by Daniel on 14/04/2019.
//  Copyright © 2019 com.danielkuta. All rights reserved.
//

import Foundation

final class ConfigurationManager: NSObject {

    // MARK: - Parametry klienta HTTP -

    private(set) public var serverProtocol: String?
    private(set) public var serverHost: String?
    private(set) public var serverPort: String?

    private(set) public var applicationContext: String?

    public var imageURLString: String { return "\(self.serverProtocol!)://\(self.serverHost!):\(self.serverPort!)/files/" }
    
    public var fileUploadURLString: String { return "\(self.serverProtocol!)://\(self.serverHost!):\(self.serverPort!)/files" }
    
    
    static public let shared = ConfigurationManager()

    // MARK: - Konstruktory -

    fileprivate override init() {
        super.init()

        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                var format = PropertyListSerialization.PropertyListFormat.xml
                let rootDictionary = try PropertyListSerialization.propertyList(from: data, options: [], format: &format) as? [String: AnyObject]

                if let networkDictionary = rootDictionary?["Network"] as? [String: AnyObject] {
                    if let serverDictionary = networkDictionary["Server"] as? [String: String] {
                        self.serverProtocol = serverDictionary["Protocol"]
                        self.serverHost = serverDictionary["Host"]
                        self.serverPort = serverDictionary["Port"]
                    }
                    
                    self.applicationContext = networkDictionary["Application context"] as? String
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
